import './App.css';

import Navigation from './components/UI/Navigation';
import { Route,Switch } from 'react-router-dom';
import GameContainer from './components/UI/GameContainter';
import About from './components/UI/About';
import Contact from './components/UI/Contact';


function App() {
  let initRows = 40;
  let initColumns = 55;

  if(window.innerWidth < 400) {
    initRows = 35;
    initColumns = 25;
  }

  return (
    <>    
    <div className="cover-container d-flex h-100 p-3 mx-auto flex-column">
      <Navigation />
      <main role="main" className="inner cover">  
      <Switch>
        <Route path="/about" component={About} />
        <Route path="/contact" component={Contact}></Route>
        <Route path="/"><GameContainer initRows={initRows} initColumns={initColumns} /></Route>
      </Switch>
      </main>

      <footer className="mastfoot mt-auto" style={{marginTop: '10% !important'}}>
        <div className="inner">
          <p>Cover template for <a href="https://getbootstrap.com/">Bootstrap</a>, by <a href="https://twitter.com/mdo">@mdo</a>.</p>
        </div>
      </footer>
    </div>
    </>
  );
}

export default App;

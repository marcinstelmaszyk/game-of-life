import React, { Component } from 'react';
import classes from './Grid.module.css';

class Grid extends Component {

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.isValid;
    }

    render(props) { 
        return (  
            <div className={classes.GridBuilder}>
                <table style={{tableLayout: 'fixed', width: '100%'}}>    
                    <tbody className={classes.GridBody}>
                    {this.props.rows}
                    </tbody>
                </table>
            </div>
        );
    }
}
 
export default Grid;
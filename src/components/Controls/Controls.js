import React from 'react';
import Slider from './Slider/Slider';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Card from 'react-bootstrap/Card'
import classes from './Controls.module.css';

const controls = (props) => {

    return (
        <Card className={classes.Controls}>
        <Container style={{textAlign: 'left'}}>
                <Form.Group>
                    <Form.Row>
                        <Col>
                            <Form.Label>Grid Rows</Form.Label>
                            <Form.Control type="number" min={props.gridConfig.minRows} max={props.gridConfig.maxRows} size="sm" name="rows" disabled={props.gameRun} value={props.gridConfig.rows} onChange={props.rowsChanged} />
                        </Col>
                        <Col>
                            <Form.Label>Grid Columns</Form.Label>
                            <Form.Control type="number" size="sm" min={props.gridConfig.minColumns} max={props.gridConfig.maxColumns} name="columns" disabled={props.gameRun} value={props.gridConfig.columns} onChange={props.columnsChanged} />
                        </Col>
                    </Form.Row>
                </Form.Group>            
                <Form.Group>
                    <Form.Row>
                        <Col style={{minWidth: '100px'}}>
                            <Button variant="primary" size="sm" disabled={props.gameRun} onClick={props.gameStarted}>Start</Button>{' '}
                            <Button variant="primary" size="sm" disabled={!props.gameRun} onClick={props.gameStopped}>Stop</Button>{' '}
                            <Button variant="primary" size="sm" disabled={props.gameRun} onClick={props.gridReset}>Reset</Button>{' '}                                        
                        </Col>
                        <Col>
                            <Slider speed={props.speed} onSpeedChange={props.onSpeedChange} />
                        </Col>
                    </Form.Row>
                </Form.Group>
        </Container>
        </Card>
      );
}
 
export default controls;
import classes from './Slider.module.css';

const slider = (props) => {
    const handleChange = e => props.onSpeedChange(e.target.value);
    return (
        <>        
        <span>Speed</span>
        <span>+</span>
        <input className={classes.Slider}
            type='range'
            min='50'
            max='1000'
            step='50'
            value={props.speed}
            onChange={handleChange}
        />
        <span>-</span>
        </>
      );
}
 
export default slider;
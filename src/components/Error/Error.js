import React, { Component } from 'react';
import Alert from 'react-bootstrap/Alert';

class Error extends Component {
       
    render(props) { 
        if(this.props.show) {
            return ( 
                <Alert variant='danger'>{this.props.children}</Alert>
            );
        }
        return <></>
    }
}
 
export default Error;
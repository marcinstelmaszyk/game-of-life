import React from 'react';

const contact = () => {
    return (  
        <>
        <h1 className="display-3">Contact me</h1>
        <p className="mb-2 text-justify">I'm passionate about software engineering. At the same time I enjoy working with others and seeing how the provided technical 
          solution works for a benefit of business. Throughout my career, communication, organizational and technical skills, allows me to run different projects, 
          playing both manager and developer roles. My education in business and IT allows me to clearly understand customer needs and communicate it clearly to the technical team. Thus, by "wearing a customer hat", 
          I'm able to provide satisfying solutions for their needs. I have a rich background in cooperation with customers and partners in the international environment.
          I'm not closing in to any particular technology. I treat technology as a tool for providing business value for customers.
        </p>
        <footer className="blockquote-footer"><cite title="Source Title">Marcin Stelmaszyk - MBA, PMP </cite></footer>
        <div className="mt-5">
          <a href="https://www.linkedin.com/in/marcinstelmaszyk/" target="_blank" rel="noreferrer" className="button linkedin"><span><i className="fa fa-linkedin" aria-hidden="true"></i></span><p>Linkedin</p></a>
          <a href="mailto: marcin.stelmaszyk@gmail.com" target="_blank" rel="noreferrer" className="button email"><span><i className="fa fa-at" aria-hidden="true"></i></span><p>E-mail</p></a>          
        </div>
        </>
    );
}
 
export default contact;
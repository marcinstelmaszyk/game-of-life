import { initial } from 'lodash';
import React from 'react';

const about = () => {
    return (  
        <>
        <h1 className="display-3">The Game of Life</h1>
        <blockquote className="blockquote" style={{fontSize: initial}}>
            <p className="mb-2 text-justify">The Game of Life, also known simply as Life, is a cellular automaton devised by the British mathematician John Horton Conway in 1970.
            It is a zero-player game, meaning that its evolution is determined by its initial state, requiring no further input. One interacts with the Game of Life by 
            creating an initial configuration and observing how it evolves. It is Turing complete and can simulate a universal constructor or any other Turing machine.</p>
            <footer className="blockquote-footer">Wikipedia <cite title="Source Title">https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life</cite></footer>
        </blockquote>
        <h1 className="display-5 mt-5">About the implementation</h1>
        <p className="text-justify">This game is a sample implementation of the Game of Life in <a href="https://reactjs.org/" className="badge badge-primary">React</a>.
          I used the game as an perfect exercise to learn React. The source code is public and can be seen in my 
          <a href="https://bitbucket.org/marcinstelmaszyk/game-of-life/src/master/" className="badge badge-primary">BitBucket repository</a>. If you spot a bug or
          want to suggest a change - go ahead and create a pull request for me. The application is automaticaly build from master branch and deployed on 
          <a href="https://aws.amazon.com/amplify/" className="badge badge-primary">AWS Aplify.</a>
        </p>
        <footer className="blockquote-footer"><cite title="Source Title">Marcin Stelmaszyk </cite></footer>
        </>
    );
}
 
export default about;
import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import GameOfLife from '../../containers/GameOfLife'

const GameContainer = (props) => {
    return ( 
        <Container fluid>
        <Row>
          <Col><GameOfLife rows={props.initRows} columns={props.initColumns} /></Col>
        </Row>
        </Container>
     );
}
 
export default GameContainer;
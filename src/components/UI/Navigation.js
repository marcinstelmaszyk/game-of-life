import React from 'react';
import { NavLink } from 'react-router-dom';

const navigation = () => {
    return (  
    <header className="masthead mb-3">
        <div className="inner">
          <h3 className="masthead-brand">Conway's game of life</h3>
          <nav className="nav nav-masthead justify-content-center">        
            <NavLink className="nav-link" to="/" exact>Home</NavLink>
            <NavLink className="nav-link" to="/about">About</NavLink>
            <NavLink className="nav-link" to="/contact">Contact</NavLink>
          </nav>
        </div>
    </header>
    );
}
 
export default navigation;
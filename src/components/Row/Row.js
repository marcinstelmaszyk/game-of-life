import React from 'react';
import Cell from '../Cell/Cell';

const row = (props) => {
    let cells = props.row.map((_,columnId) => {
        return (
            <Cell key={props.rowId.toString()+'-'+columnId} enabled={props.row[columnId] === 1 ? 'Enabled' : 'Disabled'} cellClicked={() => props.cellClicked(Number(props.rowId),Number(columnId))} />
        );
    });

    return (
        <tr>{cells}</tr>
     );
}
 
export default row;
import React from 'react';
import classes from './Cell.module.css';

const cell = (props) => {
        
     return (        
            <td className={[classes.Cell,classes[props.enabled]].join(' ')} onClick={props.cellClicked}>&nbsp;</td>
        )     
}

export default cell;
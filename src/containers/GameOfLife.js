import React, { Component } from 'react';
import GRow from '../components/Row/Row';
import Controls from '../components/Controls/Controls';
import classes from './GameOfLife.module.css';
import _ from 'lodash';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Grid from '../components/Grid/Grid';
import Error from '../components/Error/Error';
class GameOfLife extends Component {
    state = {  
        gridConfig: {
            rows: null,
            columns: null,
            minRows: 5,
            maxRows: 100,
            minColumns: 5,
            maxColumns: 100
        },
        grid: [],
        genCounter: 0,
        gameRun : false,
        speed: 500,
        isGridConfigValid: true
    }

    constructor(props) {
        super(props);

        this.state.gridConfig.rows = Number(props.rows);
        this.state.gridConfig.columns = Number(props.columns);
        this.state.grid = this.getEmptyGrid(props.rows,props.columns);
    }

    componentDidUpdate(prevProps,prevState) {
        // console.log('[ComponentDidUpdate]',this.state);
        const gameStarted = !prevState.gameRun && this.state.gameRun;
        const gameStopped = prevState.gameRun && !this.state.gameRun;
        const speedChanged = prevState.speed !== this.state.speed;

        if(gameStopped || speedChanged) {
            clearInterval(this.timerID);
        }

        if((this.state.gameRun && speedChanged) || gameStarted) {
            this.timerID = setInterval(() => {
                this.setState(state => { 
                    if(state.gameRun) {
                        const nextGenGrid = this.getNextGenerationGrid(_.cloneDeep(state.grid));
                        return ({grid: nextGenGrid, genCounter: state.genCounter+1});
                    }
                });
            },this.state.speed);
        }
        
    }

    getNeighbors(row,column) {

        const neighborsOffset = [
            [0,-1],
            [1,-1],
            [-1,-1],
            [1,0],
            [-1,0],
            [0,1],
            [-1,1],
            [1,1]
        ];

        const neighborsAddr = neighborsOffset.map(offset => {
            return [row+offset[0],column+offset[1]];
        }).filter(nAddr => {
           return (nAddr[0]>=0 && nAddr[1] >=0 && nAddr[0]<this.state.gridConfig.rows && nAddr[1] < this.state.gridConfig.columns) ? true : false;
        });
        
        return neighborsAddr;
    }

    willCellStayAlive(neighborsAddr,isAlive,genGrid) {
        const aliveNeighbors = neighborsAddr.map(nAddr => {            
            return genGrid[nAddr[0]][nAddr[1]];
        }).reduce((acc,value) => {
            return acc + value;
        });
        
        if(isAlive && (aliveNeighbors === 2 || aliveNeighbors === 3))  {
            return true;
        } else if (!isAlive && aliveNeighbors === 3) {
            return true;
        } else {
            return false;
        }
    }

    isAlive(r,c,genGrid) {
        return genGrid[r][c] === 1 ? true : false;
    }

    getNextGenerationGrid(prevGenerationGrid) {
        const nextGenGrid = this.getEmptyGrid(this.state.gridConfig.rows,this.state.gridConfig.columns);
        for(let r=0;r<prevGenerationGrid.length;r++) {
            for(let c=0;c<prevGenerationGrid[r].length;c++) {                                                    
                this.willCellStayAlive(this.getNeighbors(r,c),this.isAlive(r,c,prevGenerationGrid),prevGenerationGrid) ? nextGenGrid[r][c]=1 : nextGenGrid[r][c]=0;
            }
        }

        return nextGenGrid;
    }


    getEmptyGrid(rows, columns) {        
        const newGrid = [];
        for(let i=0;i<Number(rows);i++) {            
            newGrid.push(Array(Number(columns)).fill(0));
        }
        
        return newGrid;
    }

    gridRowsChangedHandler = (event) => {        
        const gridConfig = {...this.state.gridConfig};
        gridConfig.rows = Number(event.target.value);

        const newGrid = this.getEmptyGrid(gridConfig.rows,gridConfig.columns);       
        this.setState({gridConfig: gridConfig, 
                       grid: newGrid, 
                       isGridConfigValid: gridConfig.rows >= gridConfig.minRows && gridConfig.rows <= gridConfig.maxRows ? true : false });
    }

    gridColumnsChangedHandler = (event) => {
        const gridConfig = {...this.state.gridConfig};
        gridConfig.columns = Number(event.target.value);
        
        const newGrid = this.getEmptyGrid(gridConfig.rows,gridConfig.columns);       
        this.setState({gridConfig: gridConfig, 
                       grid: newGrid, 
                       isGridConfigValid: gridConfig.columns >= gridConfig.minColumns && gridConfig.columns <= gridConfig.maxColumns ? true : false });
    }

    cellClickedHandler = (row,column) => {        
        // do not accept clicks on the grid if game is run
        if(this.state.gameRun) {
            return;
        }

        this.setState(state => {  
            // this is 2D array. Spread operator won't work as it makes only a shallow copy                      
            const newGrid = _.cloneDeep(state.grid);
            newGrid[row][column] = newGrid[row][column] === 0 ? 1 : 0;            
            return {grid: newGrid};
        });
    }

    gridResetHandler = () => {
        const emptyGrid = this.getEmptyGrid(this.state.gridConfig.rows,this.state.gridConfig.columns);
        this.setState({grid: emptyGrid, genCounter: 0});        

        console.table(this.getNeighbors(0,4));
    }

    gameStoppedHandler = () => {
        this.setState({gameRun: false});        
    }
    
    gameStartedHandler = () => {        
        this.setState({gameRun: true});
    }

    onSpeedChangeHanlder = (newSpeed) => {
        this.setState({speed: newSpeed});
    }
        
    
    render() {                          
        const rows = this.state.grid.map((row,i) => (                    
            <GRow key={i} rowId={i} row={row} cellClicked={this.cellClickedHandler} />
        ));

        return ( 
            <Container className={classes.GameContainter}>
                <Row>
                    <Col className={classes.GridRow}>
                        <Grid rows={rows} isValid={this.state.isGridConfigValid} />
                    </Col>
                </Row>
                <Row>
                    <Col style={{marginTop: '10px', textAlign: 'left'}}><strong>Generation: </strong>{this.state.genCounter}</Col>
                </Row>
                <Error show={!this.state.isGridConfigValid}>Rows no limted to be between 5 and 100</Error>
                <Row>
                    <Col>
                        <Controls gridConfig={this.state.gridConfig}                                 
                                rowsChanged={this.gridRowsChangedHandler} 
                                columnsChanged={this.gridColumnsChangedHandler} 
                                gridReset={this.gridResetHandler} 
                                gameStarted={this.gameStartedHandler} 
                                gameStopped={this.gameStoppedHandler} 
                                gameRun={this.state.gameRun}
                                speed={this.state.speed}
                                onSpeedChange={this.onSpeedChangeHanlder} />                        
                    </Col>
                </Row>
            </Container>
            );
    }
}
 
export default GameOfLife;